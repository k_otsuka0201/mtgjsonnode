module.exports = {
  "publicPath": "./",
  "transpileDependencies": [
    "vuetify"
  ],
  pages: {
    index: {
      entry: "src/main.js",
      title: "MtgJson API",
    },
  },
  css: {
    // 毎回読み込んでおくscssファイルの指定
    loaderOptions: {
      css: {
        sourceMap: true,
      },
      scss: {
        prependData: `@import "@/assets/sass/app.scss";`,
        sourceMap: true,
      },
    }
  },
}
