import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

const getDefaultSearch = () => {
  return {
    keywords: null,
    currentpage: 1,
  }
}

Vue.use(Vuex)

export default new Vuex.Store({
  namespaced: true,
  state: {
    drawer: false,
    keywords: null,
    currentpage: 1,
  },
  mutations: {
    setDrawer(state, drawer) {
      state.drawer = drawer;
    },
    setKeywords(state, keywords) {
      state.keywords = keywords;
    },
    setCurrentpage(state, currentpage) {
      state.currentpage = currentpage;
    },
    clearSearch(state) {
      Object.assign(state, getDefaultSearch())
    }
  },
  actions: {
  },
  modules: {
  },
  plugins: [
      createPersistedState({
        key: 'mtgjsonapi',
        paths: [
            'keywords',
            'currentpage',
        ],
        storage: window.sessionStorage,
      }),
  ]
})
