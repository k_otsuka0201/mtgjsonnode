import Vue from 'vue'
import VueRouter from 'vue-router'

import Mtgjson from '../views/Mtgjson.vue'

Vue.use(VueRouter)

  const routes = [
    {
      path: '/',
      name: 'Top',
      redirect: {name: 'Mtgjson'},
    },
    {
      path: '/mtgjson',
      name: 'Mtgjson',
      component: Mtgjson,
      props: route => {
        const page = route.query.page
        const keywords = route.query.keywords
        return {
          page: /^[1-9][0-9]*$/.test(page) ? page * 1 : 1,
          keywords: keywords ? keywords : ''
        }
      }
    },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
